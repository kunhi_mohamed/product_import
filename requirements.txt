
Flask==1.0.2
Flask-Login==0.4.0
Flask-SQLAlchemy==2.3.2
Flask-WTF==0.14.2
Flask-Migrate==2.1.1

celery==4.4.7
boto3==1.14.34
smart_open==2.1.0
redis==3.2.1
pandas==1.1.0
requests==2.18.4
SQLAlchemy==1.1.14
urllib3==1.22
visitor==0.1.3
Werkzeug==0.14.1
WTForms==2.1

# requirements for Heroku
psycopg2==2.7.3.1
gunicorn==19.7.1	