
import os
import time
from datetime import datetime, timedelta
import pandas as pd
from smart_open import open

from app import celery
from flask_login import current_user
from app import db, s3
from app.models import User, Products, Jobs



@celery.task(time_limit=2*24*60*60, bind=True)
def csv_to_db(self, *fileInfo):
	"""it is a worker function whose done fetch data from csv file and sync to db"""
	print(fileInfo)
	filename = fileInfo[0]
	user_id = int(fileInfo[1])

	que_add(self.request.id, "upload", user_id)

	path = 's3://{}:{}@{}/{}'.format(os.environ['S3_KEY'], 
		os.environ['S3_SECRET'], os.environ['S3_BUCKET'], filename)

	df = pd.read_csv(open(path))

	total_rows = len(df.index)

	df = pd.read_csv(open(path), chunksize=100)
	for each in df:
		print("df")
		dict_collections = []
		duplicates_remove_helper = set({})
		for index, eachOne in each.iterrows():
			active = True if(index%2 == 0) else False		
			if str(eachOne["sku"]).strip().lower() not in duplicates_remove_helper:
				duplicates_remove_helper.add(str(eachOne["sku"]).strip().lower())
				eachOne["sku"] = str(eachOne['sku']).strip().lower()
				eachOne["name"] = str(eachOne['name'])
				eachOne["description"] = str(eachOne['description'])
				eachOne["active"] = active
				dict_collections.append(eachOne)
			# percentage stored in jobs
			self.update_state(state='PROGRESS',
							meta={'progress': 100.0 * index / total_rows})
		
		
		#collect skus as tuple and check in db		
		sku_collection = tuple([each_dict["sku"] for each_dict in dict_collections])	
		db_fetched = Products.query.filter(Products.sku.in_(sku_collection))
		update_sku_collection = {str(each_db.sku):each_db for each_db in db_fetched 
		if each_db.sku in sku_collection}

		# db sync start from here
		db_sync_collection = []
		for each_csv_dict in dict_collections:
			if(update_sku_collection.get(each_csv_dict["sku"])):				
				take_corresponding_record = update_sku_collection.get(each_csv_dict["sku"])						
				take_corresponding_record.name = each_csv_dict['name']
				take_corresponding_record.description = each_csv_dict['description']
				take_corresponding_record.active = each_csv_dict['active']
				take_corresponding_record.user_id = int(user_id) 
				db_sync_collection.append(take_corresponding_record)
			else:
				db_sync_collection.append(Products(sku=each_csv_dict['sku'], name=each_csv_dict['name'],
										description=each_csv_dict['description'], active=each_csv_dict["active"], 
										user_id=int(user_id)))			
		# mylist = list(dict.fromkeys(db_sync_collection))			
		update_db(db_sync_collection)

	self.update_state(state='PROGRESS',
					meta={'progress': 100.0 })

	que_update(self.request.id, True)
	delete_s3_file(filename)
	# os.remove(filename)

def delete_s3_file(filename):
	"""delete the s3 file after syncing"""

	s3.delete_object(Bucket=os.environ['S3_BUCKET'], Key=filename)

	print("done")


def update_db(update_data):
	"""add or update data to sync"""
	try:
		db.session.add_all(update_data)	
		db.session.commit()
		return True
	except Exception as e:
		print(str(e))
		return False

def update_db_row(**update_data):
	"""add or update one record of data"""
	try:				
		pro_data = Products.query.filter_by(sku=str(update_data["sku"]).strip().lower()).first()
		activity = True if update_data["activity_status"] == "true" else False
		if not pro_data:
			pro_record = Products(sku=str(update_data['sku']).strip().lower(), name=str(update_data['name']),
						description=str(update_data['description']), active=activity, user_id=int(update_data["user_id"]))					 
			db.session.add(pro_record)
		else:
			pro_data.name = str(update_data['name'])
			pro_data.description = str(update_data['description'])
			pro_data.active = activity
			pro_data.user_id = int(update_data["user_id"])	
		db.session.commit()

		return True
	except Exception as e:
		print(str(e))
		return False		
		

@celery.task(time_limit=2*24*60*60, bind=True)
def delete_all_task(self, user_id):
	"""delte all worker function"""
	que_add(self.request.id, "delete", user_id)
	db.session.query(Products).filter(Products.user_id==user_id).delete()
	db.session.commit()
	update_job_record = Jobs.query.filter_by(job_id=self.request.id).first()
	update_job_record.job_finish_status = True
	db.session.commit()
	print("deleted")


def que_add(que_id, job_type, user_id):
	"""collect job details"""
	print(que_id)
	job_record = Jobs(job_id=que_id, job_type=job_type, 
		date_time=datetime.utcnow()+timedelta(hours=5, minutes=30), job_finish_status=False, user_id=user_id)
	db.session.add(job_record)
	db.session.commit()
	time.sleep(1)

def que_update(que_id, status):
	"""update job details"""	
	update_job_record = Jobs.query.filter_by(job_id=que_id).first()
	update_job_record.job_finish_status = status
	db.session.commit()	