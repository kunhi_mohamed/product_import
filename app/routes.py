
import os
import time
import json
import datetime
import pandas as pd

from flask_login import current_user, login_user
from flask_login import logout_user, login_required
from flask import render_template, flash, redirect, url_for
from flask import request, current_app
from flask import jsonify
from werkzeug.urls import url_parse
# from werkzeug import secure_filename

from boto3.s3.transfer import TransferConfig
from app import db, app, s3
from app.tasks import (delete_s3_file, update_db_row, 
que_add, csv_to_db, delete_all_task)
from app.forms import LoginForm
from app.models import User, Products, Jobs



@app.route('/')
@app.route('/index')
@login_required
def index():
	"""the upload file page"""	
	return render_template('index.html', title='Home')


@app.route('/login', methods=['GET', 'POST'])
def login():
	"""authenicate user"""
	form = LoginForm()
	if form.validate_on_submit():
		user = User.query.filter_by(username=form.username.data).first()
		print(form.username.data)
		print(form.password.data)
		if user is None or not user.check_password(form.password.data):
			return redirect(url_for('login'))
		login_user(user, remember=True)
		next_page = request.args.get('next')
		if not next_page or url_parse(next_page).netloc != '':
			next_page = url_for('index')
		return redirect(next_page)			
	return render_template('login.html', title='Sign In', form=form)


@app.route('/url_generate', methods=['PUT'])
@login_required
def generate_url():
	"""generate signed url upload to s3"""
	try:
		csv_file_data = request.get_json()
		csv_file_name = csv_file_data["file_name"]
		generated_url = s3.generate_presigned_url('put_object', Params={'Bucket': os.environ['S3_BUCKET'],'Key':csv_file_name, 'ContentType': 'application/octet-stream'}, ExpiresIn=3600, HttpMethod='PUT')
		return jsonify({'status': 'success', 'url': generated_url, 'file_name': csv_file_name})
	except Exception as e:
		return jsonify({'status': 'failure', 'reason':str(e)})
	# print(response)


@app.route('/csv_process', methods=['PUT'])
@login_required
def csv_process():
	"""csv db sync precess initiater"""
	try:
		csv_file_data = request.get_json()
		csv_file_name = csv_file_data["file_name"]
		
		result = csv_to_db.delay(csv_file_name, current_user.id)
		time.sleep(1)
		return jsonify({"status":"success"})
	except Exception as e:
		print("Error csv process")
		print(str(e))
		delete_s3_file(csv_file_name)
		return jsonify({'status': 'failure'})	


@app.route('/upload_status', methods=['GET','PUT'])
@login_required
def background_job_status():
	"""to know the job status"""
	try:
		if request.method != 'PUT':
			return render_template('upload_status.html')
		else:
			data_limit_and_offset = request.get_json()
			limit = data_limit_and_offset['limit']
			offset = data_limit_and_offset['offset']

			data_collection = []
			jobs_data = Jobs.query.distinct().filter_by(user_id=current_user.id).order_by().limit(limit).offset(offset).all()

			for each_record in jobs_data:
				temp_dict = {}
				temp_dict['job_id'] = each_record.job_id
				temp_dict['date'] = str(each_record.date_time.date())
				temp_dict['time'] = str(each_record.date_time.time().strftime('%I:%M %p'))
				temp_dict['type'] = each_record.job_type
				try:
					if(not each_record.job_finish_status):
						result = csv_to_db.AsyncResult(each_record.job_id)
						print(result.info)
						if result.info.get('progress'):
							percentage = str(round(result.info.get('progress'),2))+"%"
						else:
							continue	
					else:
						percentage = "100.0%"
				except Exception as e:
					print(str(e))
					continue			
				temp_dict['percentage'] = percentage
				temp_dict['refresh'] = "disabled" if(percentage == "100.0%") else "enabled"
				data_collection.append(temp_dict)

			status = 'finished' if not jobs_data else 'unfinished'	
				
			return jsonify({'data':data_collection, 'status':status, 'limit': limit, 'offset': offset})	
	except Exception as e:
		print(str(e))
		return jsonify({'status':"finished", 'error': str(e)})			

@app.route('/upload_status_refresh', methods=['PUT'])
@login_required
def background_job_status_refresh():
	"""refreshing the job status"""
	try:
		job_data = request.get_json()
		job_id = job_data["job_id"]
		result = csv_to_db.AsyncResult(job_id)
		percentage = str(round(result.info.get('progress'),2))
		return jsonify({'percentage':percentage})
	except Exception as e:
		print(str(e))	


@app.route('/products', methods=['GET', 'PUT'])
@login_required
def products():
	"""to show the synced data"""
	try:
		if request.method != 'PUT':
			return render_template('products.html')
		else:
			data_limit_and_offset = request.get_json()
			limit = data_limit_and_offset['limit']
			offset = data_limit_and_offset['offset']

			data_collection = []
			product_data = Products.query.distinct().filter_by(user_id=current_user.id).order_by().limit(limit).offset(offset).all()



			for each_record in product_data:
				temp_dict = {}
				temp_dict['sku'] = each_record.sku
				temp_dict['name'] = each_record.name
				temp_dict['description'] = each_record.description
				temp_dict['active'] = each_record.active
				data_collection.append(temp_dict)
			# if offset>=10:
			status = 'finished' if not product_data else 'unfinished'
			return jsonify({'data':data_collection, 'status':status, 'limit': limit, 'offset': offset})
	except Exception as e:
		print(str(e))			


@app.route('/update', methods=['PUT'])
@login_required
def update():
	"""update the product data"""
	try:
		data_to_update = request.get_json()
		sku = str(data_to_update['sku']).lower()
		name = str(data_to_update['name'])
		description = str(data_to_update['description'])
		activity_status = str(data_to_update['activity']).lower()

		status = "updated" if (update_db_row(**{"sku": sku, "name": name, 
		"description": description, "activity_status": activity_status, 
		"user_id": current_user.id})) else "not updated"

		return jsonify({'status':status, "updated_data": {"sku": sku, "name": name, 
		"description": description, "activity_status": activity_status}})
	except Exception as e:
		print(str(e))
		return jsonify({'status':'not updated'})


@app.route('/delete', methods=['DELETE'])
@login_required
def delete():
	"""delete single record"""
	try:
		sku = str(request.args.get('sku')).lower()
		not_none = Products.query.filter_by(sku=sku).first()
		db.session.delete(not_none)
		db.session.commit()
		return jsonify({'status':'deleted'})
	except:
		return jsonify({'status':'not deleted'})


@app.route('/delete_all', methods=['PUT'])
@login_required
def delete_all():
	"""delete all record of corresponding user"""
	try:
		rq_job = delete_all_task.delay(current_user.id)
		time.sleep(1)
		return jsonify({"status":"deleted"})
	except Exception as e:
		print(str(e))
		return jsonify({'status':'not deleted'})


@app.route('/logout')
@login_required
def logout():
	"""logout user"""
	logout_user()
	return redirect(url_for('login'))		