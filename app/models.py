from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from app import db
from app import login

class User(UserMixin, db.Model):
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(64), index=True, unique=True)
	email = db.Column(db.String(120), index=True, unique=True)
	password_hash = db.Column(db.String(128))
	products = db.relationship('Products', backref='owner')
	jobs = db.relationship('Jobs', backref='owner')

	def set_password(self, password):
		self.password_hash = generate_password_hash(password)

	def check_password(self, password):
		return check_password_hash(self.password_hash, password)

	def __repr__(self):
		return '<User {}>'.format(self.username)  


@login.user_loader
def load_user(id):
	return User.query.get(int(id))


class Products(db.Model):
	sku = db.Column(db.String(128), primary_key=True)
	name = db.Column(db.String(128), nullable=True)
	description = db.Column(db.Text, nullable=True)
	active = db.Column(db.Boolean, default=False)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


class Jobs(db.Model):
	job_id = db.Column(db.String(128), primary_key=True)
	job_type = db.Column(db.String(128), nullable=True)	
	date_time = db.Column(db.DateTime, default=None)
	job_finish_status = db.Column(db.Boolean, default=False)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))	 			