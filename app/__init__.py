import os
import boto3
from botocore.config import Config as Config_s3
from flask import Flask
from celery import Celery
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager


app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
s3 = boto3.client(
	   's3',
	   aws_access_key_id=os.environ['S3_KEY'],
	   aws_secret_access_key=os.environ['S3_SECRET'],
	    region_name='us-east-1', 
	    config = Config_s3(signature_version = 's3v4', s3={'addressing_style': 'virtual'})
	)
migrate = Migrate(app, db)
login = LoginManager(app)
login.login_view = 'login'
celery = Celery(app.name, backend='redis', broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


from app import routes, models, tasks