"""empty message

Revision ID: ed32da5da0e5
Revises: a22aa856cc56
Create Date: 2020-08-02 12:53:29.041058

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ed32da5da0e5'
down_revision = 'a22aa856cc56'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('products', sa.Column('active', sa.Boolean(), nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('products', 'active')
    # ### end Alembic commands ###
