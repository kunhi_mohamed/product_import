import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
	SECRET_KEY = os.environ.get('SECRET_KEY')
	SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
		'postgresql:///product_import_dev'		
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	CELERY_BROKER_URL = os.environ.get('REDIS_URL')
	CELERY_RESULT_BACKEND = os.environ.get('REDIS_URL')